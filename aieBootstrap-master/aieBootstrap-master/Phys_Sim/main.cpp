#include "Phys_SimApp.h"

int main() {
	
	// allocation
	auto app = new Phys_SimApp();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}