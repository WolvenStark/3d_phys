#pragma once

#include "Application.h"
#include <glm/mat4x4.hpp>

class Camera;
namespace Physics {
	class Object;
}

class PhysicsEngineApp : public aie::Application {
public:

	PhysicsEngineApp();
	virtual ~PhysicsEngineApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	// camera transforms
	Physics::Object *m_object = nullptr;
	float m_force = 1.0f;
	Camera *m_camera = nullptr;

};