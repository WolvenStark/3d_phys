#include "PhysicsEngineApp.h"
#include "Camera.h"
#include "Gizmos.h"
#include "Input.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "physics/Object.h"

using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;
using namespace Physics;

PhysicsEngineApp::PhysicsEngineApp() {

}

PhysicsEngineApp::~PhysicsEngineApp() {

}

bool PhysicsEngineApp::startup() {
	
	setBackgroundColour(0.25f, 0.25f, 0.25f);

	// initialise gizmo primitive counts
	Gizmos::create(10000, 10000, 10000, 10000);

	// create simple camera transforms
	m_camera = new Camera();
	m_camera->SetProjection(glm::radians(45.0f), (float)getWindowWidth() / (float)getWindowHeight(), 0.1f, 1000.0f);
	m_camera->SetPosition(glm::vec3(10, 10, 10));
	m_camera->Lookat(glm::vec3(0, 0, 0));

	m_object = new Object();
	return true;
}

void PhysicsEngineApp::shutdown() {

	Gizmos::destroy();
}

void PhysicsEngineApp::update(float deltaTime) {

	m_camera->Update(deltaTime);

	// wipe the gizmos clean for this frame
	Gizmos::clear();

	// draw a simple grid with gizmos
	vec4 white(1);
	vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(vec3(-10 + i, 0, 10),
						vec3(-10 + i, 0, -10),
						i == 10 ? white : black);
		Gizmos::addLine(vec3(10, 0, -10 + i),
						vec3(-10, 0, -10 + i),
						i == 10 ? white : black);
	}

	// add a transform so that we can see the axis
	Gizmos::addTransform(mat4(1));

	// quit if we press escape
	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();

	// UP DOWN MOVEMENT
	if (input->isKeyDown(aie::INPUT_KEY_UP))
		m_object->applyForce(vec3(0, 0, m_force));

	if (input->isKeyDown(aie::INPUT_KEY_DOWN))
			m_object->applyForce(vec3(0, 0, -m_force));

	// LEFT RIGHT MOVEMENT
	if (input->isKeyDown(aie::INPUT_KEY_LEFT))
		m_object->applyForce(vec3(m_force, 0, 0));

	if (input->isKeyDown(aie::INPUT_KEY_RIGHT))
		m_object->applyForce(vec3(-m_force, 0, 0));


	m_object->update(deltaTime);
	aie::Gizmos::addSphere(m_object->getPosition(), 1, 8, 8, vec4(1, 0, 0, 1));
}

void PhysicsEngineApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// update perspective based on screen size

	Gizmos::draw(m_camera->GetProjectionView());
}