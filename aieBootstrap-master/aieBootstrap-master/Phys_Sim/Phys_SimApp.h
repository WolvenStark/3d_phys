#pragma once

#include "Application.h"
#include <glm/mat4x4.hpp>

class Phys_SimApp : public aie::Application {
public:

	Phys_SimApp();
	virtual ~Phys_SimApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	// camera transforms
	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;
};