#pragma once
#include <glm/vec3.hpp>

using glm::vec3;

namespace Physics {

class Object
{
public:
	Object();
	virtual ~Object();

	void update(float deltaTime);
	void applyForce(const glm::vec3 & force);

	// Getters
	inline const vec3 & getPosition() const { return m_position ; }
	inline const vec3 & getVelcoity() const { return m_velocity ; }
	inline const vec3 & getAcceleration() const { return m_acceleration ; }
	inline const float getMass() const { return m_mass; }

	inline void setPosition(const vec3 & pos) { m_position = pos; }
	inline void setVelocity(const vec3 & vel) { m_velocity = vel; }
	inline void setAcceleration(const vec3 & accel) { m_acceleration = accel; }
	inline void setMass(float mass) { m_mass = mass; }
	inline void setFrcition(float friction) { m_friction = friction; }
protected:
	vec3 m_position;
	vec3 m_velocity;
	vec3 m_acceleration;
	float m_mass = 1.0f;
	float m_friction = 0.3f;
private:

};

}

