#include "physics/Object.h"

using glm::vec3;
using namespace Physics;

Object::Object()
{
	m_position = vec3();
	m_velocity = vec3();
	m_acceleration = vec3();
}


Object::~Object()
{
}

void Physics::Object::update(float deltaTime)
{
	// Apply friction - (Ofor some magical reason)
	applyForce(-m_velocity * m_friction);

	// Acceleration, change in velocity overtime.
	m_velocity += m_acceleration * deltaTime;
	// Position changes based on veelocity
	m_position += m_velocity * deltaTime;

	// Reset Acceleration to zero for next frame
	m_acceleration = vec3();
}

void Physics::Object::applyForce(const glm::vec3 & force)
{
	// Force = Mass * Acceleration

	m_acceleration += force / m_mass;
}
